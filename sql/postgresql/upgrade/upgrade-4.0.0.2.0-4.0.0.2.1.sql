-- upgrade-4.0.0.2.0-4.0.0.2.1.sql
SELECT acs_log__debug('/packages/sencha-portal/sql/postgresql/upgrade/upgrade-4.0.0.2.0-4.0.0.2.1.sql','');


-- Grant Customers permission to view projects
SELECT im_grant_permission(461,41331,'view');

create or replace function inline_0 ()
returns integer as '
declare
        v_count  integer;
begin
    select count(*) into v_count from im_categories
        where category_id = 1420;
    if v_count > 0 then return 0; end if;
    INSERT INTO im_categories (category_id,category,category_type,aux_string1, aux_string2, enabled_p, parent_only_p)
VALUES (1420,''Sencha Dynviews'',''Intranet DynView Type'','''','''',''t'',''f'');
    return 0;
end;' language 'plpgsql';

select inline_0 ();
drop function inline_0 ();


-- Freelancer assigned projects dynview with columns
insert into im_views (view_id, view_name, view_label, view_type_id, view_sql)
values (1040, 'customer_portal_active_projects', 'Active projects', 1420, 'select p.* from im_projects p where p.project_status_id = 76 and p.company_id =:company_id order by p.project_id desc limit 50');

-- Build columns for new dynview customer_active_projects

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) 
values (103040,1040,NULL,'#intranet-core.Project_Type#','','im_name_from_id(p.project_type_id) as project_type_name','',5,'','project_type_name','', '');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) 
values (103042,1040,NULL,'#intranet-core.Project_Nr#','"<A HREF=/intranet/projects/view?project_id=$project_id>$project_nr</A>"','','',10,'','project_nr','', 'display:project_link');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) 
values (103044,1040,NULL,'#intranet-core.Project_Name#','"<A HREF=/intranet/projects/view?project_id=$project_id>$project_name</A>"','','',15,'','project_name','', 'display:project_link');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) 
values (103046,1040,NULL,'#intranet-core.source_language_id#','','im_name_from_id(p.source_language_id) as source_language_name','',20,'','source_language_name','', '');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) 
values (103048,1040,NULL,'#intranet-core.Target_Language#','','array_to_string(array( SELECT im_name_from_id(language_id) FROM im_target_languages langs WHERE p.project_id = langs.project_id ), '', '') AS target_language_names','',25,'','target_language_names','', '');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) 
values (103050,1040,NULL,'#intranet-core.Subject_Area#','','im_name_from_id(p.subject_area_id) as subject_area_name','',30,'','subject_area_name','', '');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) 
values (103052,1040,NULL,'#intranet-core.Role_im_project_Project_Manager#','','im_name_from_id(p.project_lead_id) as project_manager_name','',35,'','project_manager_name','', '');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) 
values (103054,1040,NULL,'#intranet-core.company_project_nr#','','','',40,'','company_project_nr','string', '');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) 
values (103056,1040,NULL,'#intranet-core.primary_contact_id#','','im_name_from_id(p.company_contact_id) as company_contact_name','',45,'','company_contact_name','', '');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) 
values (103058,1040,NULL,'#intranet-sencha-tables.price#','','','',50,'','cost_quotes_cache','', '');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) 
values (103060,1040,NULL,'#intranet-core.End_Date#','','','',55,'','end_date','', '');





