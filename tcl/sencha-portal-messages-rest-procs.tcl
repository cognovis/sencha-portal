# /packages/sencha-portal-rest-procs

ad_library {
    Rest Procedures for the sencha-portal messages and notifications package
    @author michaldrn@wp.pl
}


ad_proc -public im_rest_get_custom_unviewed_notifications {
    { -user_id ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls to get unviewed notifications

    @param user_id if of user for whom we want to fetch notifications
    
    @return object_id id of the object for the notification
    @return project_id id of project related to that notification
    @return notification_id id of the notification (primary key)
    @return object_name name of the object which is the context for the notification
    @return object_type_name name of message object type
    @return creation_date date when notification was created
    @return creation_date_formatted formatted version of creation date
    @return message notification message (its content)
    @return severity notification level of importance. 0 = red, 1 = yellow, 2 = green

} {
    
    set obj_ctr 0
    set notifications_as_list [list]

    if {$user_id eq ""} {
        set user_id $rest_user_id
    }
   
    set notifications_sql "select * from im_freelance_notifications where user_id =:user_id and viewed_date is NULL"

    db_foreach notification $notifications_sql {
       set object_type_name [acs_object_type $object_id]
       set creation_date_formatted [lc_time_fmt $creation_date "%d.%m.%Y %H:%m"]
       set project_id ""
       set object_name ""
       switch $object_type_name {
          "im_project" {
              set object_name [db_string get_project_nr "select project_nr from im_projects where project_id =:object_id" -default ""]
              set project_id $object_id
          }
          "im_freelance_assignment" {
             set object_name [db_string get_assignment_name "select assignment_name from im_freelance_assignments where assignment_id =:object_id" -default ""]
             set project_id [im_project_id_from_assignment_id -assignment_id $object_id]
          }
          "mail_log" {
            set object_name message
            set project_id ""
          }
          default {
              catch {
                  set object_name [im_name_from_id $object_id]
              }
          }
         
       }
       lappend notifications_as_list [im_rest_json_object -proc_name im_rest_get_custom_unviewed_notifications]
       incr obj_ctr
    }


    set notifications [join $notifications_as_list ","]
    set result "{\"success\": true, \"total\":$obj_ctr, \n\"data\": \[\n$notifications\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return
}




ad_proc -public im_rest_get_custom_notifications_severity {
    { -user_id ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls to get current severity level


} {
    
    set obj_ctr 0
    set notifications_as_list [list]

    if {$user_id eq ""} {
        set user_id $rest_user_id
    }

    set notifications_severity ""

    set displayed_severity -1
    set total_notifications 0

    set severity_sql "select severity, count(*) as total from im_freelance_notifications where user_id =:user_id and viewed_date is NULL group by severity order by severity desc"
    set total_rfq [db_string rfq_total "select count(*) from external_rfq" -default 0]


    db_foreach severity $severity_sql {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }

        if {$total > 0} {
            set displayed_severity $severity
            set total_notifications [expr $total_notifications + $total]
        }

        append notifications_severity "$komma{\"severity\":$severity, \"total\":$total}"
        incr obj_ctr
    }
   


    set result "{\"success\": true, \"total\":$total_notifications, \"total_rfq\":$total_rfq, \"displayed_severity\":$displayed_severity, \n\"data\": \[\n$notifications_severity\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return
}


ad_proc -public im_rest_get_custom_sencha_view_notification {
    { -notification_id ""}
    { -user_id ""}
    { -severity 2}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    
    Handler for GET request which marks notification as read

    @param notification_id id of notification which we want to mark as read. If empty, all notifications for given severity and user will be markes as read.
    @param user_id id of user (viewer)
    @param severity. Severity (importance of notification)

    @return success result of action

} {
   
    set success true

    if {$user_id eq ""} {
        set user_id $rest_user_id
    }
    
    set notification_sql ""
    if {$notification_id ne ""} {
        set notification_sql " and notification_id =:notification_id"
    }


    db_dml update_notification "update im_freelance_notifications set viewed_date = now()
        where user_id = :user_id
        and severity = :severity
        and viewed_date is null
        $notification_sql"


    set result "{\"success\": $success}"
    im_rest_doc_return 200 "application/json" $result
    return  

}


###########
# Messages
###########

    # -context_id
    # -project_id
    # -recipient_id
    # -message_body



ad_proc -public im_rest_get_custom_sencha_project_conversation {
    { -message_id ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {

    Return a list of messages from conversation.
    Conversations are just messages linked with same context_id (in this case context_id is message_id)
    Messages from same conversation should also have same subject

    @param message_id id of master message. It is usually the earliest message created.
    @return log_id log_id of saved to acs_mail_log. Needed in frontend to later fetch conversation
    @return message_id id of message saved to acs_mail log. 
    @return project_id id of project related to message
    @return project_nr Nr of project related to message
    @return project_name Name of project related to message
    @return sender_id id of message sender, stored in `acs_mail_log`
    @return sender_name sender name based on sender_id
    @return receiver_id receiver_id (recipient) of message stored in `acs_mail_log_recipient_map` table
    @return receiver_name receiver name based on receiver_id
    @return communication_partner_id It is either sender_id or receiver_id. We use such variable to make it easier on frontned sied
    @return communication_partner_name communitation partner name based on communication_partner_id
    @return sent_date sent date of message
    @return read_p boolean which indicates if message was already read
    @return subject subject of message. For conversations (Chat UI) we usually use same subject for all messages
    @return body body of message, can contain html tags
    @return avatar_url image url of avatar of person who sent message


} {

    set success_p 1
    set conversation_messages ""
    set conversation_messages_as_list [list]

    # lappend notifications_as_list [im_rest_json_object -proc_name im_rest_get_custom_unviewed_notifications]
    set messages_final [list]
    set needed_logs_ids [list]
    
    # First we need to get original message ("master message"), so we add message_id to needed_message_ids
    lappend needed_logs_ids $message_id

    # We need to get project_id from master message. We know for sure that project_id is context_id in case of earliest message
    set context_id [db_string get_project_id "select context_id from acs_mail_log where log_id =:message_id" -default ""]

    
    # With project_id we can query for project info from im_projects
    # Check if the context is a project
    switch [acs_object_type $context_id] {
        im_project {
            # With project_id we can query for project info from im_projects
            db_1row project_info "select project_name, project_nr, project_id from im_projects where project_id = :context_id"
        }
        im_trans_invoice - im_invoice {
            # Get the project info based on the context of the cost item
            db_1row project_info "select p.project_id, p.project_name, p.project_nr from im_projects p, im_costs c where p.project_id = c.project_id and c.cost_id = :context_id"
        }
    }

    set log_ids_list [db_list get_log_ids "select log_id from acs_mail_log where context_id =:message_id"]
    set needed_logs_ids [concat $needed_logs_ids $log_ids_list]

    set conversation_sql "select * from acs_mail_log ml, acs_mail_log_recipient_map rm where rm.log_id = ml.log_id and ml.log_id in ([ns_dbquotelist $needed_logs_ids]) order by sent_date asc"
    
    db_foreach message $conversation_sql {
        
        # For some reason I named variable receiver instead of recipient, gonna fix that later
        set receiver_id $recipient_id

        set read_p [db_string message_read_p "select 1 from views_views where object_id = :log_id and viewer_id = :rest_user_id" -default 0]
	if {$sender_id ne ""} {
	    set sender_name [im_name_from_id $sender_id]
	} else {
	    set sender_name "unknown"
	}
        set receiver_name [im_name_from_id $recipient_id]

        set avatar_file [im_portrait_user_file $sender_id]
        if {$avatar_file eq ""} {
            set avatar_url ""
        } else {
            set avatar_url "[ad_url]/sencha-portal/download/user/$sender_id/$avatar_file"
        }

        if {$sender_id eq $rest_user_id} {
            set communication_partner_id $recipient_id
            set communication_partner_name $receiver_name
        } 

        if {$recipient_id eq $rest_user_id} {
            set communication_partner_id $sender_id
            set communication_partner_name $sender_name               
        }
    
        if {![info exists communication_partner_id]} {
            set communication_partner_id $recipient_id
            set communication_partner_name $receiver_name
        }

        # Files support
        set download_files ""
        # Append files
        db_foreach files {
            select cr.title, cr.revision_id as version_id from acs_mail_log_attachment_map lam, cr_revisions cr
                where log_id = :log_id
                and cr.revision_id = lam.file_id
        } {
            append download_files "<a href=\"[export_vars -base "[im_rest_system_url]/[apm_package_url_from_key "intranet-mail"]download/$title" -url {version_id}]\">$title</a><br>"
        }

        if {$download_files ne ""} {
            set body "$body <p/> $download_files"
        }

        lappend conversation_messages_as_list [im_rest_json_object -proc_name im_rest_get_custom_sencha_project_conversation]
        incr obj_ctr
    }
    
    set conversation_messages [join $conversation_messages_as_list ","]
    set result "{\"success\": $success_p, \"total\":[llength $needed_logs_ids],\"project_id\":\"$project_id\",\n\"data\": \[\n$conversation_messages\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
}


ad_proc -public im_rest_post_object_type_sencha_portal_project_message {
    { -content ""}
    { -subject ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs "" }
    { -debug 0 }
} {
    Send message from portals.
    New version of im_rest_post_object_type_sencha_portal_send_message, now used toghether with Converations.
    Special thing about conversations is that we generally use earliest message_id as context_id
    If message_id (master message) is null it means we are staring new conversation (usually with same subject)


    @param message_id id of parent (master message). Saved to database as context_id
    @param project_id Project id in which one we want to communicate. Can be either PM, Freelancer of Company Contact, depending on sender role in project.
    @param recipient_id Recipient of message. User_id of receiver. For now it can only be single value.
    @param subject subject of new message (message topic). It is important for frontend, because we use it
    @param message_body body of new message, can contain html tags.

} {

    array set query_hash [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]

    set required_vars [list context_id project_id recipient_id subject message_body]
    foreach var $required_vars {
        if {![exists_and_not_null query_hash($var)]} {
            im_rest_error -format $format -http_status 406 -message "Variable '$var' not specified. The following variables are required: $required_vars"
            #sencha_errors::add_error -object_id $random_errors_token -field "$var"  -problem "this is required field"
        } else {
            set $var $query_hash($var)
        }
    }

    set sender_id $rest_user_id
    set locale [lang::user::locale -user_id $rest_user_id]

    set log_id [im_freelance_message_send \
        -sender_id $sender_id \
        -recipient_ids $recipient_id  \
        -subject "$subject" \
        -body $message_body \
        -context_id $context_id \
        -project_id $project_id]

    im_freelance_record_view -object_id $log_id -viewer_id $rest_user_id

    set modal_title "[lang::message::lookup $locale  sencha-portal.message_sent_title]"
    set modal_message "[lang::message::lookup $locale sencha-portal.message_sent_message]"


    set result "{\"success\": 1, \"content\":\"[im_quotejson $message_body]\",\"message\":\"$modal_message\",\"modal_title\":\"$modal_title\"}"
    im_rest_doc_return 200 "application/json" $result

    set hash_array(rest_oid) $log_id
    set hash_array(result) $result

    return [array get hash_array]
    
}

ad_proc -public im_rest_post_object_type_sencha_portal_send_message {
    { -content ""}
    { -subject ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs "" }
    { -debug 0 }
} {
    Send message from portals.

    @param context_id Context is needed for 'im_freelance_message_send' procedure. In this particular case this one is gonna be either project_id or message_id.
    @param project_id Project id in which one we want to communicate. Can be either PM, Freelancer of Company Contact, depending on sender role in project.
    @param recipient_id Recipient of message. User_id of receiver. For now it can only be single value.
    @param subject subject of new message (message topic). It is important for frontend, because we use it
    @param message_body body of new message, can contain html tags.

} {

    array set query_hash [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]

    set required_vars [list context_id project_id recipient_id subject message_body]
    foreach var $required_vars {
        if {![exists_and_not_null query_hash($var)]} {
            im_rest_error -format $format -http_status 406 -message "Variable '$var' not specified. The following variables are required: $required_vars"
            #sencha_errors::add_error -object_id $random_errors_token -field "$var"  -problem "this is required field"
        } else {
            set $var $query_hash($var)
        }
    }

    set sender_id $rest_user_id
    set locale [lang::user::locale -user_id $rest_user_id]

    set log_id [im_freelance_message_send \
        -sender_id $sender_id \
        -recipient_ids $recipient_id  \
        -subject "$subject" \
        -body $message_body \
        -context_id $context_id \
        -project_id $project_id]

    im_freelance_record_view -object_id $log_id -viewer_id $rest_user_id

    set modal_title "[lang::message::lookup $locale  sencha-portal.message_sent_title]"
    set modal_message "[lang::message::lookup $locale sencha-portal.message_sent_message]"


    set result "{\"success\": 1, \"content\":\"[im_quotejson $message_body]\",\"message\":\"$modal_message\",\"modal_title\":\"$modal_title\"}"
    im_rest_doc_return 200 "application/json" $result

    set hash_array(rest_oid) $log_id
    set hash_array(result) $result

    return [array get hash_array]

}


ad_proc -public im_rest_post_object_type_sencha_portal_send_conversation_message {
    { -content ""}
    { -subject ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs "" }
    { -debug 0 }
} {
    Send message from portals.

    @param context_id Context is needed for 'im_freelance_message_send' procedure. In this particular case this one is gonna be either project_id or message_id.
    @param project_id Project id in which one we want to communicate. Can be either PM, Freelancer of Company Contact, depending on sender role in project.
    @param message_body body of new message, can contain html tags.

} {

    array set query_hash [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]

    set required_vars [list context_id message_body]
    foreach var $required_vars {
        if {![exists_and_not_null query_hash($var)]} {
            im_rest_error -format $format -http_status 406 -message "Variable '$var' not specified. The following variables are required: $required_vars"
            #sencha_errors::add_error -object_id $random_errors_token -field "$var"  -problem "this is required field"
        } else {
            set $var $query_hash($var)
        }
    }

    set locale [lang::user::locale -user_id $rest_user_id]

    db_1row master_message_details "select context_id as project_id, subject, sender_id as master_message_sender_id, recipient_id as master_message_recipient_id from acs_mail_log ml, acs_mail_log_recipient_map rm where rm.log_id = ml.log_id and ml.log_id=:context_id"

    set sender_id $rest_user_id
    set recipient_id ""
    
    # If we have sender_id, we can figure out receiver_id (recipient)
    if {$sender_id eq $master_message_sender_id} {
        set recipient_id $master_message_recipient_id
    } else {
        set recipient_id $master_message_sender_id
    }
    
    # Send message
    set log_id [im_freelance_message_send \
        -sender_id $sender_id \
        -recipient_ids $recipient_id  \
        -subject "$subject" \
        -body $message_body \
        -context_id $context_id \
        -project_id $project_id]

    im_freelance_record_view -object_id $log_id -viewer_id $rest_user_id

    set modal_title "[lang::message::lookup $locale  sencha-portal.message_sent_title]"
    set modal_message "[lang::message::lookup $locale sencha-portal.message_sent_message]"


    # Notify recipient of new messag
    # We must set 'master_message_id', sender_name' and 'project_nr', as they are both needed in 'sencha-portal.new_message_notification'
    set sender_name [im_name_from_id $rest_user_id]
    set master_message_id $context_id
    set project_nr [db_string get_project_name "select project_nr from im_projects where project_id = :project_id" -default ""]
    set notification_content "[_ sencha-portal.new_message_notification]"
    # We pass master_message_id as object_id to be able to redirect user directly to conversation in portals

    im_freelance_notify \
        -object_id $context_id \
        -recipient_ids $recipient_id \
        -message $notification_content \
        -severity 1

    set result "{\"success\": true, \"content\":\"[im_quotejson $message_body]\",\"message\":\"$modal_message\",\"modal_title\":\"$modal_title\"}"
    im_rest_doc_return 200 "application/json" $result

    set hash_array(rest_oid) $log_id
    set hash_array(result) $result

    return [array get hash_array]

}

ad_proc -public im_rest_get_custom_sencha_open_message {
    -message_id
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    
    Handler for GET request to save message as read

} {

    set success true
    im_freelance_record_view -object_id $message_id -viewer_id $rest_user_id

    set result "{\"success\": $success}"
    im_rest_doc_return 200 "application/json" $result
    return  

}

ad_proc -public im_rest_get_custom_sencha_possible_message_recipients {
    -project_id
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    
    Handler for GET request which gets possible message recipients for user

} {

    set success true
    set recipients ""
    set recipients_sql ""
    set recipients_ids_list [list]
    set show_recipient_role_p 0

    if {[im_user_is_customer_p $rest_user_id]} {
        set recipients_sql "select person_id, first_names, last_name, email from cc_users u, im_projects p where p.project_lead_id = u.person_id and p.project_id =:project_id"
    }

    if {[im_user_is_pm_p $rest_user_id]} {
        set show_recipient_role_p 1
        set all_project_possible_recipients_ids [list]
        # First we get ids of freelancers with assignments within current project
        set freelancers_in_project_sql "select distinct assignee_id from im_freelance_assignments fa, im_freelance_packages fp where fa.freelance_package_id = fp.freelance_package_id and fp.project_id =:project_id"
        set freelancers_in_project_ids [db_list get_project_freelancers $freelancers_in_project_sql]
        # Then we also get company contact
        set company_contact_id [db_string get_company_contact_id "select company_contact_id from im_projects where project_id=:project_id" -default ""]
        if {$company_contact_id ne ""} {
            # We want company contact to always show first
            lappend all_project_possible_recipients_ids $company_contact_id
        }
        # Next we add all freelancers
        foreach freelancer_id $freelancers_in_project_ids {
            lappend all_project_possible_recipients_ids $freelancer_id
        }
        if {[llength $all_project_possible_recipients_ids] > 0} {
            set recipients_sql "select u.person_id, u.first_names, u.last_name, u.email, (select count(company_contact_id) from im_projects where project_id =:project_id and company_contact_id=u.person_id) as is_project_contact from cc_users u where user_id in ([ns_dbquotelist $all_project_possible_recipients_ids]) order by is_project_contact DESC"
        }
    }

    if {[im_user_is_freelance_p $rest_user_id]} {
        set recipients_sql "select person_id, first_names, last_name, email from cc_users u, im_projects p where p.project_lead_id = u.person_id and p.project_id =:project_id"
    }

    if {$recipients_sql ne ""} {
        set obj_ctr 0
        db_foreach recipient $recipients_sql {
            set komma ",\n"
            if {0 == $obj_ctr} { set komma "" }
            if {![info exists is_project_contact]} {
                set is_project_contact 0
            }
            set full_name "$first_names $last_name"
            if {$show_recipient_role_p} {
                if {$is_project_contact} {
                    set full_name "$first_names $last_name (customer)"
                } else {
                    set full_name "$first_names $last_name (freelancer)"
                }
            }
            
            append recipients "$komma{\"object_id\":$person_id, \"user_id\":$person_id,\"first_names\":\"$first_names\",\"last_name\":\"$last_name\",\"full_name\":\"$full_name\",\"email\":\"$email\",\"is_project_contact\":$is_project_contact}"
            incr obj_ctr
        }
    }

    set result "{\"success\": $success,\n\"data\": \[\n$recipients\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return  

}


ad_proc -public im_rest_get_custom_sencha_project_messages {
    { -project_id ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {

    Return a list of messages of a project

    @param project_id to which project messages are related


} {
    set success true
    set messages ""
    set user_id $rest_user_id
    set user_email [im_email_from_user_id $user_id]
    set locale [lang::user::locale -user_id $rest_user_id]

    set obj_ctr 0

    # Getting user company_id 
    set company_id [im_user_main_company_id -user_id $rest_user_id]
    
    set company_contacts_sql "select cc_users.user_id from acs_rels, cc_users where cc_users.user_id = acs_rels.object_id_two and object_id_one = :company_id  and rel_type = 'im_company_employee_rel' and acs_rels.object_id_two not in (
                -- Exclude banned or deleted users
                select  m.member_id
                from    group_member_map m,
                    membership_rels mr
                where   m.rel_id = mr.rel_id and
                    m.group_id = acs__magic_object_id('registered_users') and
                    m.container_id = m.group_id and
                    mr.member_state != 'approved'
                ) order by cc_users.first_names asc"

    set recipients_ids [db_list get_company_contacts_ids $company_contacts_sql]
    lappend recipients_ids $rest_user_id
    
    # Get the project_lead just in case
    set project_lead_id [db_string project_lead "select project_lead_id from im_projects where project_id = :project_id" -default ""]

    if {$recipients_ids eq ""} {
        set success false
        set result "{\"success\": $success}"
        im_rest_doc_return 200 "application/json" $result
    } else {

        if {$project_id ne ""} {
            # Append the invoices of this project
            set cost_ids [db_list costs "select cost_id from im_costs, im_invoices where project_id = :project_id and cost_id = invoice_id"]
            set context_ids [concat $project_id $cost_ids]

	    if {[im_user_is_pm_p $rest_user_id]} {
		if {$project_lead_id ne ""} {
		    lappend recipients_ids $project_lead_id
		}
	    }

            set messages_sql "select distinct * from acs_mail_log ml, acs_mail_log_recipient_map rm where rm.log_id = ml.log_id  \ 
            and (rm.recipient_id in ([ns_dbquotelist $recipients_ids]) or ml.sender_id in ([ns_dbquotelist $recipients_ids]) or ml.to_addr in (select email from parties where party_id = :rest_user_id)) \
            and context_id in ([ns_dbquotelist $context_ids]) order by sent_date desc"
            db_1row project_info "select project_name, project_nr from im_projects where project_id = :project_id"
        } else {
            set messages_sql "select * from acs_mail_log ml, acs_mail_log_recipient_map rm, im_projects p where rm.log_id = ml.log_id and ml.context_id = p.project_id \ 
                and (rm.recipient_id in ([ns_dbquotelist $recipients_ids]) or ml.sender_id in ([ns_dbquotelist $recipients_ids])) order by sent_date desc"
        }

        db_foreach message $messages_sql {
            set komma ",\n"
            if {0 == $obj_ctr} { set komma "" }
            #set avatar_url [im_portrait_user_file $sender_id]
            set avatar_file [im_portrait_user_file $sender_id]
            if {$avatar_file eq ""} {
                set avatar_url ""
            } else {
                set avatar_url "[ad_url]/sencha-portal/download/user/$sender_id/$avatar_file"
            }
            set read_p [db_string message_read_p "select 1 from views_views where object_id = :log_id and viewer_id = :rest_user_id" -default 0]
	    if {$sender_id ne ""} {
		set sender_name [im_name_from_id $sender_id]
	    } else {
		set sender_name "unknown"
		set sender_id "null"
	    }
            set receiver_name [im_name_from_id $recipient_id]

            set download_files ""
            # Append files
            db_foreach files {
                select cr.title, cr.revision_id as version_id from acs_mail_log_attachment_map lam, cr_revisions cr
                    where log_id = :log_id
                    and cr.revision_id = lam.file_id
            } {
                append download_files "<a href=\"[export_vars -base "[im_rest_system_url]/[apm_package_url_from_key "intranet-mail"]download/$title" -url {version_id}]\">$title</a><br>"
            }

            if {$download_files ne ""} {
                set body "$body <p/> $download_files"
            }

            if {$sender_id eq $rest_user_id || $sender_id eq $project_lead_id} {
                set communication_partner_id $recipient_id
                set communication_partner_name $receiver_name
            } 

            if {$recipient_id eq $rest_user_id || $recipient_id eq $project_lead_id} {
                set communication_partner_id $sender_id
                set communication_partner_name $sender_name               
            }
	    
    	    if {![info exists communication_partner_id]} {
    		    set communication_partner_id $recipient_id
    		    set communication_partner_name $receiver_name
    	    }

            set last_conversation_message [db_string get_last_conversation_message "select body from acs_mail_log where context_id =:log_id order by sent_date desc limit 1" -default ""]

            append messages "$komma{\"log_id\":$log_id,\"message_id\":\"$message_id\",\"package_id\":$package_id,\"project_id\":$project_id,\"project_nr\":\"$project_nr\",\"project_name\":\"$project_name\",\"sender_id\":$sender_id,\"sender_name\":\"$sender_name\",\"receiver_id\":$recipient_id,\"receiver_name\":\"$receiver_name\",\"communication_partner_id\":$communication_partner_id,\"communication_partner_name\":\"$communication_partner_name\",\"sent_date\":\"$sent_date\",\"read_p\":$read_p,\"from_addr\":\"$from_addr\",\"to_addr\":\"$to_addr\",\"subject\":\"$subject\",\"body\":\"[im_quotejson $body]\",\"download_files\":\"[im_quotejson $download_files]\",\"avatar_url\":\"[im_quotejson $avatar_url]\",\"last_conversation_message\":\"[im_quotejson $last_conversation_message]\"}"
            incr obj_ctr
        }
        

        set result "{\"success\": $success,\n\"data\": \[\n$messages\n\]\n}"
        im_rest_doc_return 200 "application/json" $result
    }
    return  
}

