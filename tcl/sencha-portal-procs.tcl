
ad_library {
    Helper procedures for sencha-portal package

    @author michaldrn@wp.pl

}

ad_proc -public im_user_main_company_id {
    -user_id
} {

    Return the main company id for given user_id.
    This should return single id or empty string
  
    @return company_id

} {
    # First we check if user is not primary_contact_id for 
    set company_id [db_string company_id_sql "select company_id from im_companies where primary_contact_id =:user_id limit 1" -default ""]
    # If company_id is still null, we keep on searching, this time with acs_rels
    if {$company_id eq ""} {
        set company_id [db_string company_id_sql "select object_id_one as company_id from acs_rels where object_id_two =:user_id and rel_type = 'im_company_employee_rel' limit 1" -default ""]
    }
    return $company_id

}


ad_proc -public im_company_key_account_id {
    -company_id
} {

    Return the key account id for given company_id
    This should return single id or empty string
  
    @return key_account_id

} {
    # First we check if user is not manager_id
    set key_account_id [db_string key_account_id_sql "select manager_id from im_companies where company_id =:company_id" -default ""]
    # If company id is null, we check acs_rels
    if {$company_id eq ""} {
        set key_account_id [db_string key_account_id_sql "select object_id_two as key_account_id from acs_rels where object_id_one =:company and rel_type = 'im_key_account_rel' limit 1" -default ""]
    }
    return $key_account_id

}


ad_proc -public im_category_ids_to_names {
    -category_ids
} {

    Converts list of category ids to list of category names

} {
    set category_names [list]
    foreach category_id $category_ids {
        lappend category_names [im_name_from_id $category_id]
    }

    return $category_names
}


ad_proc -public im_verify_user_token {
    -email
    -token
} {

    Procedure used to verify user token used for password reset

} {
    
    set password_token_matched_p [db_string user_id_from_email "select 1 from cc_users where email =:email and password =:token" -default 0]

    return $password_token_matched_p
    
}


ad_proc -public im_get_column_name_of_category {
    -column_name
} {
    Procedure which returns coressponding column name.
    So in case we have project_status_id it returns project_status_name (and opposite)
} {

    set value_to_return ""
    set obj_ctr 0
    set temp_list [list]

    set splitted_column_name [split $column_name "_"]
    set last_one [lindex $splitted_column_name [expr [llength $splitted_column_name] -1]]
    foreach column_part $splitted_column_name {
        if {[expr [llength $splitted_column_name] -1] > $obj_ctr} {
            lappend temp_list $column_part
        }
        incr obj_ctr
    }

    if {$last_one eq "name"} {
        lappend temp_list "id"
    }
    if {$last_one eq "id"} {
        lappend temp_list "name"
    }

    return [join $temp_list "_"]

}

ad_proc -public sencha_folder_permissions {
    -project_id
    -user_id
    {-type "read"}
} {
    Returns allowed folders relative to project

    @param project_id ID of the project for which we check the permission
    @param user_id ID of the user for whom to check permission
    @param type Type of permission requested. Defaults to "read"
    
    @return allowed_folders List of folders (full path) the user is allowed to access
} {

    # Check that the user is allowed within the project
    im_project_permissions $user_id $project_id view read write admin

    if {!$read} {
        # Check if the user maybe is working in a company which has access to the project.
        set company_id [db_string company "select company_id from im_projects where project_id = :project_id"]
        set read [im_biz_object_member_p $user_id $company_id]
        set write 1
    }

    if {!$read} {
        # Check if the user is a freelancer in the project
        # Do this by checking the assignments
        set read [db_string assignment_exists "select 1 from im_freelance_packages fp, im_freelance_assignments fa where fa.freelance_package_id = fp.freelance_package_id and fp.project_id = :project_id and fa.assignee_id = :user_id limit 1" -default 0]

        # Freelancer are never allowed to write directly into folders
        set write 0
    }

    set allowed_folders [list]
    set project_path [cog::project::path -project_id $project_id]

    if {$type eq "read" && $read} {
        if {[im_user_is_pm_p $user_id] || [im_user_is_admin_p $user_id]} {
            # get a list of all the folders in the project

            set folder_files [ad_find_all_files -include_dirs 1 $project_path]
            
            # Remove requested_path from list
            set idx [lsearch $folder_files $project_path]
            set folder_files [lreplace $folder_files $idx $idx]

            foreach file $folder_files {
                if {[file isfile $file]} {
                    set dir_name [file dirname $file]
                } else {
                    set dir_name $file
                }
                if {[lsearch allowed_folders $dir_name]<0} {
                    lappend allowed_folders $dir_name
                }
            }
        } else {
	    set allowed_folders_param [list]

            if {[im_user_is_customer_p $user_id]} {
                set allowed_folders_param  [parameter::get_from_package_key -package_key "sencha-portal" -parameter "CustomersFolder" -default ""]
            } 
            if {[im_user_is_freelance_p $user_id]} {
                set allowed_folders_param  [parameter::get_from_package_key -package_key "sencha-portal" -parameter "FreelancersFolder" -default ""]
            }
            
            foreach allowed_folder_par $allowed_folders_param {
                set allowed_folder "${project_path}/$allowed_folder_par"

                # Avoice duplicates
                if {[lsearch $allowed_folders $allowed_folder]<0} {
                    lappend allowed_folders $allowed_folder
                } 
            }
        }
    }

    # Handle write folders
    if {$type eq "write" && $write} {
        if {[im_user_is_customer_p $user_id]} {
            set allowed_folders_param  [parameter::get_from_package_key -package_key "sencha-portal" -parameter "CustomersFolder" -default ""]
            set not_allowed_write [list "Final"]
            foreach allowed_folder_par $allowed_folders_param {
                if {[lsearch $not_allowed_write $allowed_folder_par]<0} {
                    set allowed_folder "${project_path}/$allowed_folder_par"
                    # Avoice duplicates
                    if {[lsearch $allowed_folders $allowed_folder]<0} {
                        lappend allowed_folders $allowed_folder
                    }
                }
            } 
        }
        
        if {[im_user_is_pm_p $user_id] || [im_user_is_admin_p $user_id]} {
            # get a list of all the folders in the project

            set folder_files [ad_find_all_files -include_dirs 1 $project_path]
            
            # Remove requested_path from list
            set idx [lsearch $folder_files $project_path]
            set folder_files [lreplace $folder_files $idx $idx]

            foreach file $folder_files {
                if {[file isfile $file]} {
                    set dir_name [file dirname $file]
                } else {
                    set dir_name $file
                }
                if {[lsearch allowed_folders $dir_name]<0} {
                    lappend allowed_folders $dir_name
                }
            }
        }
    }
    return $allowed_folders
}

# Register download procedure
ad_proc sencha_portal_project_download {} { 
    sencha_portal_download "project" 
}

ad_proc sencha_portal_user_download {} { 
    sencha_portal_download "user" 
}

proc sencha_portal_download { folder_type } {
    
    set url "[ns_conn url]"

    set path_list [split $url {/}]
    set len [expr [llength $path_list] - 1]

    # Using the group_id as selector for various storage types.
    # skip: +0:/ +1:sencha-portal, +2:download, +3:folder_type, +4:<object_id>, +5:...
    set group_id [lindex $path_list 4]

     # Start retreiving the path starting at:
    set start_index 5

    set file_comps [lrange $path_list $start_index $len]
    set file_name [join $file_comps "/"]


    set base_path [im_filestorage_base_path $folder_type $group_id]

    if {"" == $base_path} {
        return
    }

    set file "$base_path/$file_name"

    catch { set file_readable [file readable $file] }
    


    if ($file_readable) {
	ns_log Notice "[im_name_from_id [ad_conn user_id]] from [ad_conn peeraddr] downloaded $file_name from [im_name_from_id $group_id]"
	set project_lead_id [db_string lead_from_group "select project_lead_id from im_projects where project_id = :group_id" -default ""]
	if {$project_lead_id ne ""} {
	    intranet_chilkat::send_mail \
		-to_party_ids $project_lead_id \
		-from_party_id $project_lead_id \
		-subject "Customer downloaded file" \
		-body "[im_name_from_id [ad_conn user_id]] from [ad_conn peeraddr] downloaded $file_name from [im_name_from_id $group_id]" \
		-no_callback 
	}
        rp_serve_concrete_file $file
    } else {
        return
    }
}
